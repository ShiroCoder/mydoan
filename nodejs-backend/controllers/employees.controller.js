const Employees = require('../models/employee');
const Response = require('./response');

module.exports.getAllEmployeeOfProject = (req, res) => {
    let project_id = req.body.project_id;
    Employees.find({
        project_id: project_id
    }, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseFailed({}, 'Failed to get get employees!', res.statusCode));
        } else {
            res.json(Response._ResponseSuccess(data, 'Get employee successfully!', res.statusCode));
        }
    }).sort({
        employee_id: 1
    });

};

module.exports.getEmployeeById = (req, res) => {
    let project_id = req.body.project_id;
    let employee_id = req.body.employee_id;
    Employees.findOne({
        project_id: project_id,
        employee_id: employee_id
    }, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseFailed({}, 'Failed to get get employees!', res.statusCode));
        } else {
            res.json(Response._ResponseSuccess(data, 'Get employee successfully!', res.statusCode));
        }
    }).sort({
        employee_name: 1
    });

}


module.exports.createEmployee = (req, res) => {
    let newEmployeeData = req.body.data;
    Employees.create(newEmployeeData, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseFailed({}, 'Failed to create employee'));
        } else {
            res.json(Response._ResponseSuccess(data, 'Create employee successfully'));
        }
    });
};

module.exports.updateEmployee = (req, res) => {
    let updateEmployeeData = req.body.data;
    let employee_id = updateEmployeeData.employee_id;
    Employees.findOneAndUpdate({
        employee_id: employee_id
    }, {
        $set: updateEmployeeData
    }, {
        upsert: true
    }, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseFailed({}, 'Failed to update employee'));
        } else {
            res.json(Response._ResponseSuccess(data, 'Update employee successfully'));
        }

    });
};

module.exports.deleteEmployee = (req, res) => {
    let employee_id = req.body.data;
    Employees.findOneAndDelete({
        employee_id: employee_id
    }, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseFailed({}, 'Failed to delete employee'));
        } else {
            res.json(Response._ResponseSuccess(data, 'Update delete successfully'));
        }

    });
}