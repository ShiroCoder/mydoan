const Logs = require('../models/logs-work');
const Response = require('./response');
const Employee = require('../models/employee');
    
module.exports.createLogs = (req, res) => {
    let newLogsData = req.body.data;
    Logs.create(newLogsData, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseFailed({}, "Failed to create log!"));
        } else {
            res.json(Response._ResponseSuccess(data, "Create log successfully"));
        }
    });
};

module.exports.updateLogs = (req, res) => {
    let updateLogData = req.body.data;
    let employee_id = req.body.employee_id;
    let log_id = req.body.log_id;
    Logs.findOneAndUpdate({
        log_id: log_id,
        employee_id: employee_id
    }, {
        $set: updateLogData
    }, {
        upsert: true
    }, (err, data) => {
        if (err) {
        console.log(err);
         res.json(Response._ResponseFailed({}, "Failed to update log."));
        }
        else {
            res.json(Response._ResponseSuccess(data, "Update log successfully."));
        }
    });
};

module.exports.getLogs = (req, res) => {
    let employee_id = req.body.employee_id;
    Logs.find({employee_id}, (err, data) => {
        if (err) {
            res.json(Response._ResponseFailed({},'Failed to get data'));
        }      
        else {
            res.json(Response._ResponseSuccess(data, 'Get data successfully.'));
        }
    });
};