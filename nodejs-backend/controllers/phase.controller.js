const Phase = require('../models/phases');
const Response = require('../controllers/response');
const moment = require('moment');


module.exports.getAllPhases = (req, res) => {
    let project_id = req.body.project_id;
    Phase.find({
        project_id: project_id
    }, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponsePhaseFailed({}, 'Cannot find any result'));
        } else {
            res.json(Response._ResponsePhaseSuccess(data, `Find successfully!`));
        }
    }).sort({
        phase_id: 1
    });

};

module.exports.getPhaseById = (req, res) => {
    let project_id = req.body.project_id;
    let phase_id = req.body.phase_id;
    Phase.findOne({
        project_id: project_id,
        phase_id: phase_id
    }, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponsePhaseFailed({}, 'Cannot find any phase'));
        } else {
            res.json(Response._ResponsePhaseSuccess({
                data
            }, `Find successfully!`));
        }
    });
};
module.exports.getCurrentPhase = (req, res) => {
    let project_id = req.body.project_id;
    let status = "In Progress";
    Phase.find({
        project_id: project_id,
        status: status
    }, (err, data) => {
        if (err) {
            console.log(err);
            
            res.json(Response._ResponsePhaseFailed({}, 'Cannot find any phase')
            );
        } else {
            res.json(Response._ResponsePhaseSuccess({
                data
            }, `Find successfully!`));
        }
    });
};

module.exports.createPhase = (req, res) => {
    let newPhaseData = req.body.data;
    console.log(newPhaseData);
    Phase.create(newPhaseData, (err, data) => {
        var dataResponse = data;
        if (err) {
            console.log(err);
            res.json(Response._ResponsePhaseFailed({}, 'Failed to create Data'));
        } else {
            res.json(Response._ResponsePhaseSuccess(dataResponse, 'Create Successfully'));
        }

    });
};

module.exports.updatePhase = (req, res) => {
    let updatePhaseData = req.body.data;
    let phase_id = req.body.data.phase_id;
    Phase.findOneAndUpdate({
        phase_id: phase_id
    }, {
        $set: updatePhaseData
    }, {
        upsert: true
    }, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponsePhaseFailed({}, `Failed to update`));
        } else {
            res.json(Response._ResponsePhaseSuccess(data), 'Update successfully');
        }
    });
};