const Project = require('../models/project');
const Response = require('./response');
const Employee = require('../models/employee');
module.exports.getProjectInfo = (req, res) => {
    let pm_id = req.body.pm_id;
    Project.findOne({
        pm_id: pm_id
    }, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseProjectFailed({}, 'Failed to retrieve data'), res.statusCode);
        } else {
            res.json(Response._ResponseProjectSuccess(data, `Get project successfully!`, res.statusCode));
        }
    });
};


module.exports.updateProject = (req, res) => {
    let updateProjectData = req.body.data;
    let project_id = req.body.data.project_id;
    let pm_id = req.body.pm_id;
    Project.findByIdAndUpdate({
        project_id: project_id,
        pm_id: pm_id
    }, {
        $set: updateProjectData
    }, {
        upsert: true
    }, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseProjectFailed({}, 'Failed to update data'), res.statusCode);
        }
        else {
            res.json(Resposnse._ResponseProjectSuccess({}, 'Update data successfully!'));
        }
    });
};

module.exports.createProject = (req, res) => {
    let pm_id = req.body.pm_id;
    let newProjectData = req.body.data;
    Employee.findOne({employe_id: pm_id, role: 'Project Manager'}, newProjectData, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseProjectFailed({}, 'Failed to create data!'), res.statusCode);
        }
        else {
         res.json(Response._ResponseProjectSuccess(data, `Create data successfully!`, res.statusCode));
        }
    });
};