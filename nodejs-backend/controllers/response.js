module.exports = {
    _ResponsePhaseFailed: function (data, message) {
        return {
            "status": 500,
            "message": message,
            "data": data ? data : {}
        };
    },
    _ResponsePhaseSuccess: function (data, message) {
        return {
            "status": 200,
            "message": message,
            "data": data ? data : {}
        };
    },
    _ResponseTaskFailed: function (data, message) {
        return {
            "status": 500,
            "message": message,
            "data": data ? data : {}
        };
    },
    _ResponseTaskSuccess: function (data, message) {
        return {
            "status": 200,
            "message": message,
            "data": data ? data : {}
        };
    },
    _ResponseSubTaskFailed: function (data, message) {
        return {
            "status": 500,
            "message": message,
            "data": data ? data : {}
        };
    },
    _ResponseSubTaskSuccess: function (data, message) {
        return {
            "status": 200,
            "message": message,
            "data": data ? data : {}
        };

    },
    _ResponseProjectFailed: function (data, message, statusCode) {
        return {
            "status": statusCode,
            "message": message,
            "data": data ? data : {}
        };
    },
    _ResponseProjectSuccess: function (data, message, statusCode) {
        return {
            "status": statusCode,
            "message": message,
            "data": data ? data : {}
        };
    },
    _ResponseFailed: function (data, message, statusCode) {
        return {
            "status": statusCode,
            "message": message,
            "data": data ? data : {}
        };
    },
    _ResponseSuccess: function (data, message, statusCode) {
        return {
            "status": statusCode,
            "message": message,
            "data": data ? data : {}
        };
    }
};