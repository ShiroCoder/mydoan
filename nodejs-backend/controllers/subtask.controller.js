const SubTask = require('../models/subtask');
const Response = require('../controllers/response');


module.exports.getSubTask = (req, res) => {
    SubTask.find({}, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseSubTaskFailed({}, 'Failed to retrive data'));
        } else {
            res.json(Response._ResponseSubTaskSuccess(data, `Get sub-task successfully!
            Result:`));
        }
    });
};

module.exports.updateSubTask = (req, res) => {
    let subTaskData = req.body.data;
    let subtask_id = subTaskData.subtask_id;
    let task_id = subTaskData.task_id;
    SubTask.findOneAndUpdate({
        subtask_id: subtask_id,
        task_id: task_id
    }, {
        $set: subTaskData
    }, {
        upsert: true
    }, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseSubTaskFailed({}, 'Failed to update data'));
        } else {
            res.json(Resposne._ResponseSubTaskSuccess(data, `Update data successfully!
               Result:`));
        }
    });
};

module.exports.createSubTask = (req, res) => {
    let newSubTaskData =  req.body.data;
    SubTask.create(newSubTaskData, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseSubTaskFailed({}, "Failed to create data"));
        }
        else {
            res.json(Response._ResponseSubTaskSuccess(data,'Create subtask successfully'));
        }
    });
};

module.exports.deleteSubTask = (req, res) => {
    let subtask_id = req.body.data.subtask_id;
    SubTask.findOneAndDelete({subtask_id: subtask_id}, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseSubTaskFailed({}, "Failed to delete"));
        }
        else {
            res.json(Response._ResponseSubTaskSuccess(data, 'Delete successfully!'));
        }
    })
}