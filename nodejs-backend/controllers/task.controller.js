const Tasks = require('../models/tasks');
const Response = require('../controllers/response');
const randomString = require('randomstring');

module.exports.createTasks = (req, res) => {
    let newTasksData = req.body.data;
    let project_id = newTasksData.project_id;
    let phase_id = newTasksData.phase_id;
    newTasksData.task_id = `${project_id}.${phase_id}.${taskIdGenerator()}`;
    newTasksData.status = "Available";
    console.log(newTasksData);

    Tasks.create(newTasksData, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponsePhaseFailed({}, 'Failed to create daata'));
        } else {
            res.json(Response._ResponsePhaseSuccess(data, 'Create Successfully'));
        }

    });

};

module.exports.deleteTask = (req, res) => {
    console.log(req.body);
    let task_id = req.body.data.task_id;
    console.log(task_id);
    Tasks.findOneAndDelete({
        task_id: task_id
    }, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponsePhaseFailed({}, 'Failed to create daata'));
        } else {
            res.json(Response._ResponsePhaseSuccess(data, 'Task has been deleted'));
        }

    });

};

module.exports.getAllTaskByEmployeeId = (req, res) => {
    let employee_id = req.body.employee_id;
    let phase_id = req.body.phase_id;

    Tasks.find({
        employee_id: employee_id,
        phase_id: phase_id
    }, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseTaskFailed({}, 'Failed to retrieve data'));
        } else {
            res.json(Response._ResponseTaskSuccess(data, `Get data successfully
            Result:`));
        }
    });
};

module.exports.updateTask = (req, res) => {

    let updateTaskData = req.body.data;
    let task_id = updateTaskData.task_id;
    Tasks.findOneAndUpdate({
        task_id: task_id
    }, {
        $set: updateTaskData
    }, {
        $upsert: true
    }, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseTaskFailed({}, `Cannot update data`));
        } else {
            res.json(Response._ResponseTaskSuccess(data, `Update successfully`));
        }
    });
};

// module.exports.getInProgressTaskByEmployeeId = (req, res) => {
//     let employee_id = req.body.employee_id;
//     Tasks.find({
//         employee_id: employee_id,
//         status: 'In progress'
//     }, (err, data) => {
//         if (err) {
//             console.log(err);
//             res.json(Response._ResponseTaskFailed({}, `Cannot retrieve any result`));
//         } else {
//             res.json(Resposne._ResponseTaskSuccess(data, `Get data successfully`));
//         }
//     });

// };


module.exports.getAvailableTasks = (req, res) => {
    let project_id = req.body.project_id;
    let phase_id = req.body.phase_id;
    Tasks.find({
        phase_id: phase_id,
        project_id: project_id,
        status: 'Available'
    }, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseTaskFailed({}, `Cannot retrieve any result`));
        } else {
            res.json(Response._ResponseTaskSuccess(data, `Get data successfully`));
        }
    });

};

module.exports.getAllTasks = (req, res) => {
    let project_id = req.body.project_id;
    let phase_id = req.body.phase_id;
    Tasks.find({
        project_id: project_id,
        phase_id: phase_id
    }, (err, data) => {
        if (err) {
            console.log(err);
            res.json(Response._ResponseTaskFailed({}, `Cannot retrieve any result`));
        } else {
            res.json(Response._ResponseTaskSuccess(data, `Get data successfully`));
        }
    });

};

function taskIdGenerator() {
    return task_id = randomString.generate(3);
}