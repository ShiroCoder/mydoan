var mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),
    bcrypt = require('bcrypt'),
    User = mongoose.model('User');

exports.register = function (req, res) {
    var newUser = new User(req.body);
    newUser.hash_password = bcrypt.hashSync(req.body.password, 10);
    newUser.save(function (err, user) {
        if (err) {
            return res.status(400).send({
                message: err
            });
        } else {
            console.log('Registration successfully!');
            user.hash_password = undefined;
            return res.json(user);
        }
    });
};

exports.sign_in = function (req, res) {
    User.findOne({
        email: req.body.email
    }, function (err, user) {
        if (err) throw err;
        if (!user) {
            res.status(401).json({
                message: 'Authentication failed. User not found.'
            });
        } else if (user) {
            if (!user.comparePassword(req.body.password)) {
                res.status(401).json({
                    message: 'Authentication failed. Wrong password.'
                });
            } else {    
                return res.json({
                    email: user.email,
                    fullName: user.full_name,
                    employee_id: user.employee_id,
                    role: user.role,
                    project_id: user.project_id,
                    token: jwt.sign({
                        email: user.email,
                        full_name: user.full_name,
                        _id: user._id
                    }, 'RESTFULAPIs')
                });
            }
        }
    });
};



exports.loginRequired = function (req, res, next) {
    if (req.user) {
        next();
    } else {
        return res.status(401).json({
            message: 'Unauthorized user!'
        });
    }
};  