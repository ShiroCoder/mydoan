const mongoose  = require('mongoose');


let contractSchema = new mongoose.Schema({
    contract_id: {
        type: String,
        unique: true,
          required: true

    },
    employee_id:{
        type: String,
        unique: true,
          required: true

    },
    signed_date: {
        type: String,
          required: true

    },
    expired_date:{
        type: String,
          required: true

    },
    salary: {
      type: String,
      required: true
    }
});

module.exports = mongoose.model('contractSchema', contractSchema);