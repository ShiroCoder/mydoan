const mongoose = require('mongoose');


let employeeSchema = new mongoose.Schema({

    employee_id: {
        type: String,
        unique: true,
        required: true
    },
    employee_name: {
        type: String,
        trim: true,
        required: true
    },
    address: {
        type: String
    },
    gender: {
        type: String
    },
    department: {
        type: String
    },
    phone: {
        type: String
    },
    email: {
        type: String,
        unique: true
    },
    project_id: {
        type: String,
        required: true
    }
});
module.exports = mongoose.model('employeeSchema', employeeSchema);