const mongoose = require('mongoose');

logWorkSchema = new mongoose.Schema({
    task_id: {
        type:String,
          required: true

    },
    subtask_id: {
        type: String,
    },
    log_id: {
        type: String,
          required: true
    },
    log_summary: {
        type: String
    },
    log_type: {
        type: String
    },
    log_time: {
        type: String
    },
    employee_id: {
        type: String
    }

});

module.exports = mongoose.model('logWorkSchema', logWorkSchema);

