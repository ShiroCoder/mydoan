const mongoose = require('mongoose');



let phaseSchema = new mongoose.Schema({
    project_id: {
        type: String,
          required: true

    },
    phase_id: {
        type: String,
          required: true

    },
    description: {
        type: String,
        trim: true
    },
    status:{
        type: String,
          required: true

    },
    start_day: {
        type: String,
        unique: true
    },
    end_day: {
        type: String,
        unique: true
    }

});

module.exports = mongoose.model('phaseSchema', phaseSchema);