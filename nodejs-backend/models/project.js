const mongoose = require('mongoose');



let projectSchema = new mongoose.Schema({
    project_id: {
        type: String,
        trim: true,
        unique: true,
        required: true

    },
    project_name: {
        type: String,
        trim: true,
          required: true

    },
    pm_id: {
        type: String,
          required: true

    },
    description: {
        type: String,
        trim: true
    },
    startDay: {
        type: String
    },
    endDay: {
        type: String
    }

});

module.exports = mongoose.model('projectSchema', projectSchema);