const mongoose = require('mongoose');


let subTaskSchema = new mongoose.Schema({
    task_id: {
        type: String,
    },
    project_id:{
        type: String
    },
    subtask_id:{
        type: String,
        unique: true
    },
    subtask_name: {
        type: String,
        trim: true
    },
    employee_id: {
        type: String
    },
    description: {
        type: String,
        trim: true
    },
    estimated_time: {
        type: Number,
        trim: true
    },
    logged_time: {
        type: Number,
    }

});

module.exports = mongoose.model('subTaskSchema', subTaskSchema);