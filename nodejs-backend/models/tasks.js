const mongoose = require('mongoose');
const validate = require('mongoose-validator');


let taskSchema  = new mongoose.Schema({
    project_id: {
        type: String,
    },
    phase_id: {
    type: String,
      },
    task_id: {
        type: String,
        unique: true
    },
    employee_id: {
        type: String
    },
    task_name: {
        type: String,
        trim: true
    },
    description: {
        type: String,
        trim: true
    },
    estimated_time: {
        type: Number,
        trim: true
    },
    status: {
        type: String,
    },
    priority:{
        type: String
    },
    log_time: {
        type: Number,
        trim: true
    }

});

module.exports = mongoose.model('taskSchema', taskSchema);