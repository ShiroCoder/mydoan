const bodyParser = require('body-parser');
const phaseController = require('../controllers/phase.controller');
const taskController = require('../controllers/task.controller');
const employeeController = require('../controllers/employees.controller');
const userController = require('../controllers/user.controller')
const jsonParser = bodyParser.json();
module.exports = (app) => {
    //phase routing
    app.post('/login/', jsonParser, userController.sign_in);
    app.post('/register/', jsonParser, userController.register);
    app.post('/phases/getphasebyid/', jsonParser, phaseController.getPhaseById);
    app.post('/phases/getallphase/', jsonParser, phaseController.getAllPhases);
    app.post('/phases/updatephase/', jsonParser, phaseController.updatePhase);
    app.post('/phases/createphase/', jsonParser, phaseController.createPhase);
    app.post('/phases/currentphase/', jsonParser, phaseController.getCurrentPhase);
    //tasks routing
    app.post('/phases/createtasks/', jsonParser, taskController.createTasks);
    app.post('/phases/tasks/', jsonParser, taskController.getAllTasks);
    app.post('/employees/getemployeetasks/', jsonParser, taskController.getAllTaskByEmployeeId);
    app.post('/employees/getavailabletask/', jsonParser, taskController.getAvailableTasks);
    app.post('/employees/updatetasks/', jsonParser, taskController.updateTask);
    app.post('/phases/deletetask/', jsonParser, taskController.deleteTask);

    //employee routing
    app.post('/employees/createemployee/', jsonParser, employeeController.createEmployee);
    app.post('/employees/', jsonParser, employeeController.getAllEmployeeOfProject);
    //sub-task routing


}